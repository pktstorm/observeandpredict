using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ObserveAndPredict.Utils;

namespace ObserveAndPredict.UnitTests.Utils
{
    [TestFixture]
    public class CsprngUtilsTests
    {
        #region Test Case Sources

        private static IEnumerable<TestCaseData> _nextRandomIntInRangeTestCases
        {
            get
            {
                // Standard ranges
                yield return new TestCaseData(0, 10, 0, 10);
                yield return new TestCaseData(-10, 0, -10, 0);
                yield return new TestCaseData(-10, 10, -10, 10);
                yield return new TestCaseData(0, int.MaxValue, 0, int.MaxValue);
                yield return new TestCaseData(int.MinValue, 0, int.MinValue, 0);
                yield return new TestCaseData(int.MinValue, int.MaxValue, int.MinValue, int.MaxValue);

                // Reversed range
                yield return new TestCaseData(10, 0, 0, 10);
                yield return new TestCaseData(0, -10, -10, 0);

                // Exact range
                yield return new TestCaseData(0, 0, 0, 0);
                yield return new TestCaseData(-10, -10, -10, -10);
                yield return new TestCaseData(10, 10, 10, 10);
            }
        }

        #endregion Test Case Sources

        /// <summary>
        /// Validates that the NextRandomInt for a min->max range properly returns a value
        /// within the provided range. This method should never throw an exception and so should be able to handle
        /// unsanitized input.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="expectedMinBoundary"></param>
        /// <param name="expectedMaxBoundary"></param>
        [Test, TestCaseSource(nameof(_nextRandomIntInRangeTestCases))]
        public void EvaluateNextIntInRange(int minValue, int maxValue, int expectedMinBoundary, int expectedMaxBoundary)
        {
            var result = 0;
            Assert.DoesNotThrow(() => result = CsprngUtils.NextRandomInt(minValue, maxValue));
            Assert.GreaterOrEqual(result, expectedMinBoundary);
            Assert.LessOrEqual(result, expectedMaxBoundary);
        }

        /// <summary>
        /// Validates that there is at least a somewhat distributed number of random values
        /// across a wide sampling of the boolean value generator.
        /// </summary>
        [Test]
        public void AssureRandomGenerationIsDistributed()
        {
            var booleanValues = new List<bool>();
            for (var i = 0; i < 10000; ++i)
            {
                booleanValues.Add(CsprngUtils.NextRandomBoolean());
            }

            int numberOfFalse = booleanValues.Count(x => !x);
            int numberOfTrue = booleanValues.Count - numberOfFalse;

            Assert.GreaterOrEqual(numberOfFalse, 1000);
            Assert.GreaterOrEqual(numberOfTrue, 1000);
        }
    }
}