using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace ObserveAndPredict.Training
{
    /// <inheritdoc />
    [SuppressMessage("ReSharper", "UnusedMember.Global"),
     SuppressMessage("ReSharper", "UnusedMethodReturnValue.Global"),
     SuppressMessage("ReSharper", "UnusedType.Global"),
     SuppressMessage("ReSharper", "StaticMemberInGenericType")]
    public class ApiModelBuilder<TCsvModel, TPredictionModel> : IApiModelBuilder
        where TCsvModel : class, new()
        where TPredictionModel : class, IModelPredictionResult, new()
    {
        #region Fields

        private readonly MLContext _mlContext = new MLContext(0);

        private ITransformer _model;

        private readonly object _lock = new object();

        private IDataView _evaluationDataSet;

        private IDataView _trainingDataSet;

        private static readonly IDictionary<Tuple<Type, Type>, IApiModelBuilder> _singletonInstances =
            new Dictionary<Tuple<Type, Type>, IApiModelBuilder>();

        private PredictionEngine<TCsvModel, TPredictionModel> _predictionFunction;

        private bool _isTrainedAndReady;

        private double _rSquared;

        private double _rootMeanSquaredError;

        #endregion Fields

        #region Properties

        /// <inheritdoc />
        public bool IsTrainedAndReady
        {
            get
            {
                lock (_lock)
                {
                    return _isTrainedAndReady;
                }
            }
        }

        /// <inheritdoc />
        public RegressionMetrics ModelProperties { get; private set; }

        /// <inheritdoc />
        public long TimeToTrainAndEvaluateModelInMs { get; private set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Singleton manager for model builders for a particular API method.
        /// </summary>
        /// <returns></returns>
        public static IApiModelBuilder Instance()
        {
            Tuple<Type, Type> key = Tuple.Create(typeof(TCsvModel), typeof(TPredictionModel));
            if (!_singletonInstances.ContainsKey(key))
            {
                _singletonInstances[key] = new ApiModelBuilder<TCsvModel, TPredictionModel>();
            }

            return _singletonInstances[key];
        }

        #endregion Constructors

        public static void DeleteSingleton()
        {
            Tuple<Type, Type> key = Tuple.Create(typeof(TCsvModel), typeof(TPredictionModel));
            if (_singletonInstances.ContainsKey(key))
            {
                _singletonInstances.Remove(key);
            }
        }

        /// <inheritdoc />
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public ErrorStatus TrainModel(IEnumerable<string> featureNames, string labelColumn,
            IEnumerable<string> hotEncodeFields)
        {
            if (IsTrainedAndReady)
            {
                return new ErrorStatus(HttpStatusCode.AlreadyReported, "The model is already trained and ready.");
            }

            lock (_lock)
            {
                // If currently populated, clear all model state
                _model = null;

                if (_trainingDataSet == null)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest, "The training data has not yet been populated.");
                }

                var stopwatch = Stopwatch.StartNew();

                IEstimator<ITransformer> pipeline = _mlContext.Transforms.CopyColumns("Label", labelColumn);
                pipeline = hotEncodeFields.Aggregate(pipeline,
                    (current, hotEncodeField) =>
                        current.Append(
                            _mlContext.Transforms.Categorical.OneHotEncoding($"{hotEncodeField}Encoded",
                                hotEncodeField)));

                pipeline = pipeline.Append(_mlContext.Transforms.Concatenate("Features",
                    featureNames.Select(featureName =>
                        hotEncodeFields.Any(hotEncodeField =>
                            string.Compare(featureName, hotEncodeField, StringComparison.OrdinalIgnoreCase) == 0)
                            ? $"{featureName}Encoded"
                            : featureName).ToArray()));
                pipeline = pipeline.Append(_mlContext.Regression.Trainers.FastTree());
                _model = pipeline.Fit(_trainingDataSet);
                _predictionFunction = _mlContext.Model.CreatePredictionEngine<TCsvModel, TPredictionModel>(_model);


                IDataView predictions = _model.Transform(_evaluationDataSet);
                RegressionMetrics metrics = _mlContext.Regression.Evaluate(predictions);
                _rSquared = metrics.RSquared;
                _rootMeanSquaredError = metrics.RootMeanSquaredError;

                if (_rSquared < 0.5 || _rSquared > 1.5)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest,
                        $"The model has a bad RSquared value (far from 1.0): {_rSquared}");
                }

                stopwatch.Stop();
                TimeToTrainAndEvaluateModelInMs = stopwatch.ElapsedMilliseconds;
                ModelProperties = metrics;

                _isTrainedAndReady = true;
                return new ErrorStatus(HttpStatusCode.OK,
                    $"New model has been trained with RSquared of {_rSquared:n2} and RootMeanSquaredError of {_rootMeanSquaredError:n2}.");
            }
        }

        /// <inheritdoc />
        public ErrorStatus LoadTrainingDataSetFromCsv(string dataPath) =>
            LoadDataSetFromCsv(view => _trainingDataSet = view, dataPath);

        /// <inheritdoc />
        public ErrorStatus LoadEvaluationDataSetFromCsv(string dataPath) =>
            LoadDataSetFromCsv(view => _evaluationDataSet = view, dataPath);

        /// <summary>
        /// Reads in training data in CSV format to the specified data model and then relays it to the
        /// provided population lambda.
        /// </summary>
        /// <param name="populateDataSet"></param>
        /// <param name="dataPath"></param>
        /// <returns></returns>
        private ErrorStatus LoadDataSetFromCsv(Action<IDataView> populateDataSet, string dataPath)
        {
            if (IsTrainedAndReady)
            {
                return new ErrorStatus(HttpStatusCode.AlreadyReported, "The model is already trained and ready.");
            }

            lock (_lock)
            {
                if (_mlContext == null)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest, "The ML Context has not yet been constructed.");
                }

                try
                {
                    populateDataSet(
                        _mlContext.Data.LoadFromTextFile<TCsvModel>(dataPath, hasHeader: true, separatorChar: ','));
                    return new ErrorStatus(HttpStatusCode.OK,
                        $"Successfully loaded dataset from path: {dataPath}");
                }
                catch (Exception exception)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest,
                        $"The provided data could not be parsed into the data model: {exception.Message}");
                }
            }
        }

        /// <inheritdoc />
        public ErrorStatus SaveModelAsArchive(string outputPath)
        {
            if (!IsTrainedAndReady)
            {
                return new ErrorStatus(HttpStatusCode.AlreadyReported, "The model has not yet been trained.");
            }

            lock (_lock)
            {
                if (_mlContext == null)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest, "The ML Context has not yet been constructed.");
                }

                if (_model == null)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest, "The model has not yet been trained.");
                }

                if (_trainingDataSet == null)
                {
                    return new ErrorStatus(HttpStatusCode.BadRequest,
                        "The Data Training Set has not yet been populated.");
                }

                _mlContext.Model.Save(_model, _trainingDataSet.Schema, outputPath);
                return new ErrorStatus(HttpStatusCode.OK, $"The model has been saved to: {outputPath}");
            }
        }

        /// <inheritdoc />
        public ErrorStatus Predict(object obj, out float predictionResult)
        {
            if (!IsTrainedAndReady)
            {
                predictionResult = -1.0f;
                return new ErrorStatus(HttpStatusCode.AlreadyReported, "The model has not yet been trained.");
            }

            if (!(obj is TCsvModel apiCall))
            {
                predictionResult = -1.0f;
                return new ErrorStatus(HttpStatusCode.BadRequest,
                    $"The input was not of the expected type: {typeof(TCsvModel).Name}");
            }

            TPredictionModel prediction = _predictionFunction.Predict(apiCall);
            predictionResult = prediction.GetPredictionResult();
            return new ErrorStatus(HttpStatusCode.OK, "Performed prediction!");
        }
    }
}