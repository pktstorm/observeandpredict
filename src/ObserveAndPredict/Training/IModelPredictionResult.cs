using System.Diagnostics.CodeAnalysis;

namespace ObserveAndPredict.Training
{
    [SuppressMessage("ReSharper", "UnusedMember.Global"),
     SuppressMessage("ReSharper", "UnusedType.Global")]
    public interface IModelPredictionResult
    {
        /// <summary>
        /// Retrieves the result of a model prediction from a prediction container.
        /// </summary>
        float GetPredictionResult();
    }
}