using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.ML.Data;

namespace ObserveAndPredict.Training
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global"),
     SuppressMessage("ReSharper", "NotAccessedField.Global"),
     SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class ApiModelRow
    {
        [LoadColumn(0)]
        public string ApiName;

        [LoadColumn(1)]
        public string Route;

        [LoadColumn(2)]
        public string Method;

        [LoadColumn(3)]
        public float Param1;

        [LoadColumn(4)]
        public float Param2;

        [LoadColumn(5)]
        public float Param3;

        [LoadColumn(6)]
        public float Param4;

        [LoadColumn(7)]
        public float IsSuspicious;

        public static ApiModelRow FromItemCollection(IList<string> itemCollection) =>
            new ApiModelRow
            {
                ApiName = itemCollection[0],
                Route = itemCollection[1],
                Method = itemCollection[2],
                Param1 = Convert.ToInt32(itemCollection[3]),
                Param2 = Convert.ToInt32(itemCollection[4]),
                Param3 = Convert.ToInt32(itemCollection[5]),
                Param4 = Convert.ToInt32(itemCollection[6])
            };
    }
}