using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.ML.Data;

namespace ObserveAndPredict.Training
{
    /// <summary>
    /// Provides a regression model builder for training a model to evaluate for suspicious API interactions.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global"),
     SuppressMessage("ReSharper", "UnusedMethodReturnValue.Global")]
    public interface IApiModelBuilder
    {
        /// <summary>
        /// Indicates if this model is trained and ready to use for predictions.
        /// </summary>
        bool IsTrainedAndReady { get; }

        /// <summary>
        /// Retrieves information about the trained model.
        /// </summary>
        RegressionMetrics ModelProperties { get; }

        /// <summary>
        /// Indicates how long it took, in milliseconds, to train
        /// and then evaluate the model.
        /// </summary>
        long TimeToTrainAndEvaluateModelInMs { get; }

        /// <summary>
        /// Trains the model against the current set of training data.
        /// </summary>
        /// <param name="featureNames">The names of all of the columns from the dataset to use as features in the model.</param>
        /// <param name="labelColumn">Identifies the column to be labeled (read: predicted) by the model.</param>
        /// <param name="hotEncodeFields">Identifies fields that need to be re-encoded using OneHotEncoding. That service, provided by
        /// ML.net, enables converting non-mathematically-obvious fields (like strings) into values
        /// that the model can use to make statistical decisions.</param>
        /// <returns></returns>
        ErrorStatus TrainModel(IEnumerable<string> featureNames, string labelColumn,
            IEnumerable<string> hotEncodeFields);

        /// <summary>
        /// Loads a training set into
        /// </summary>
        /// <param name="dataPath"></param>
        ErrorStatus LoadTrainingDataSetFromCsv(string dataPath);

        /// <summary>
        /// Loads a training set into
        /// </summary>
        /// <param name="dataPath"></param>
        ErrorStatus LoadEvaluationDataSetFromCsv(string dataPath);

        /// <summary>
        /// Saves the model out as an archive to the specific path.
        /// </summary>
        /// <param name="outputPath"></param>
        ErrorStatus SaveModelAsArchive(string outputPath);

        /// <summary>
        /// Form a prediction about the given input using the model.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="predictionResult"></param>
        /// <returns></returns>
        ErrorStatus Predict(object obj, out float predictionResult);
    }
}