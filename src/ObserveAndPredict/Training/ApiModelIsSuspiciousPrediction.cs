using System.Diagnostics.CodeAnalysis;
using Microsoft.ML.Data;

namespace ObserveAndPredict.Training
{
    [SuppressMessage("ReSharper", "UnassignedField.Global"),
     SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class ApiModelIsSuspiciousPrediction : IModelPredictionResult
    {
        [ColumnName("Score")]
        public float IsSuspicious;

        /// <inheritdoc />
        public float GetPredictionResult() => IsSuspicious;
    }
}