using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using System.Text;

namespace ObserveAndPredict.Utils
{
    /// <summary>
    /// Utility methods for generating random data.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global"),
     SuppressMessage("ReSharper", "UnusedType.Global"),
     SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public static class CsprngUtils
    {
        #region Fields

        private static readonly RNGCryptoServiceProvider _csprng = new RNGCryptoServiceProvider();

        private const string _alphaStringChars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private const string _numericStringChars = "1234567890";

        private const string _alphaNumericStringChars = _alphaStringChars + _numericStringChars;

        #endregion Fields

        /// <summary>
        /// Generates a random integer using a CSPRNG.
        /// </summary>
        /// <returns></returns>
        public static int NextRandomInt()
        {
            var randomData = new byte[4];
            _csprng.GetBytes(randomData);
            return BitConverter.ToInt32(randomData, 0) & (int.MaxValue - 1);
        }

        /// <summary>
        /// Generates a random integer within the given range using a CSPRNG.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        [SuppressMessage("ReSharper", "InvertIf")]
        public static int NextRandomInt(int minValue, int maxValue)
        {
            if (minValue == maxValue)
            {
                return minValue;
            }
            if (minValue > maxValue)
            {
                int temp = minValue;
                minValue = maxValue;
                maxValue = temp;
            }
            return (int)Math.Floor(minValue + ((double)maxValue - minValue) * NextRandomDouble());
        }

        /// <summary>
        /// Generates a random double using a CSPRNG.
        /// </summary>
        /// <returns></returns>
        public static double NextRandomDouble()
        {
            var data = new byte[sizeof(uint)];
            _csprng.GetBytes(data);
            var randUint = BitConverter.ToUInt32(data, 0);
            return randUint / (uint.MaxValue + 1.0);
        }

        /// <summary>
        /// Generates a random string within the given length (inclusive) using a CSPRNG.
        /// </summary>
        /// <param name="minLength"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string NextAlphaNumericString(int minLength, int maxLength) =>
            NextAlphaNumericString(NextRandomInt(minLength, maxLength));

        /// <summary>
        /// Generates a random string within the given length (inclusive) using a CSPRNG.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string NextAlphaNumericString(int length) =>
            NextCompositionalString(_alphaNumericStringChars, length);

        /// <summary>
        /// Generates a random string of only latin alphabet characters within the given length (inclusive)
        /// using a CSPRNG.
        /// </summary>
        /// <param name="minLength"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string NextAlphaString(int minLength, int maxLength) =>
            NextAlphaString(NextRandomInt(minLength, maxLength));

        /// <summary>
        /// Generates a random string of only latin alphabet characters within the given length (inclusive)
        /// using a CSPRNG.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string NextAlphaString(int length) =>
            NextCompositionalString(_alphaStringChars, length);

        /// <summary>
        /// Generates a random numeric string within the given length (inclusive) using a CSPRNG.
        /// </summary>
        /// <param name="minLength"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string NextNumericString(int minLength, int maxLength) =>
            NextNumericString(NextRandomInt(minLength, maxLength));

        /// <summary>
        /// Generates a random numeric string within the given length (inclusive) using a CSPRNG.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string NextNumericString(int length) =>
            NextCompositionalString(_numericStringChars, length);

        /// <summary>
        /// Builds a new string using the provided source for acceptable characters.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private static string NextCompositionalString(string source, int length)
        {
            var result = new StringBuilder();
            var uintBuffer = new byte[sizeof(uint)];

            while (length-- > 0)
            {
                _csprng.GetBytes(uintBuffer);
                var num = BitConverter.ToUInt32(uintBuffer, 0);
                result.Append(source[(int)(num % (uint)source.Length)]);
            }

            return result.ToString();
        }

        /// <summary>
        /// Generates a true or false value using a CSPRNG. Theoretically, there is no
        /// reliable determination on the approach to a consistent distribution of values.
        /// </summary>
        /// <returns></returns>
        public static bool NextRandomBoolean() => NextRandomDouble() < 50 / 100.0;

        /// <summary>
        /// Randomly sort list using a CSPRNG.
        /// </summary>
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                var box = new byte[1];
                do
                {
                    _csprng.GetBytes(box);
                } while (!(box[0] < n * (byte.MaxValue / n)));
                int k = box[0] % n;
                --n;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }
    }
}