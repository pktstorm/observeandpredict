using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace ObserveAndPredict.Utils
{
    /// <summary>
    /// Constructs training information for use in training regression models against API methods.
    /// Note: This is totally not generic and multi-purpose...
    /// Note: This only really works against string methods now, but could be extended by using a registry against types.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedType.Global"),
     SuppressMessage("ReSharper", "UnusedMember.Global")]
    public static class TrainingUtils
    {
        /// <summary>
        /// Builds training headers for labeling columns in csv output
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> GetTrainingHeaders(int numberOfParams)
        {
            var result = new List<string>
            {
                "Controller", "Route", "Method"
            };
            for (var index = 0; index < numberOfParams; ++index)
            {
                result.Add($"Param{index + 1}");
            }

            result.Add("IsSuspicious");
            return result;
        }

        /// <summary>
        /// Builds training values for the given controller's method.
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="route"></param>
        /// <param name="method"></param>
        /// <param name="numberOfParams"></param>
        /// <param name="numberOfGood"></param>
        /// <param name="numberOfBad"></param>
        /// <param name="goodValueGenerator"></param>
        /// <param name="badValueGenerator"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<string>> GetTrainingValues(string controllerName, string route,
            string method, int numberOfParams, int numberOfGood, int numberOfBad, Func<string> goodValueGenerator, Func<string> badValueGenerator)
        {
            for (var index = 0; index < numberOfGood; ++index)
            {
                var nextResult = new List<string>
                {
                    controllerName, route, method
                };
                IEnumerable<string> paramValues = EnumerableUtils.NewOfGenerated(numberOfParams, goodValueGenerator);
                nextResult.AddRange(paramValues);
                nextResult.Add(0.0f.ToString(CultureInfo.InvariantCulture));
                yield return nextResult;
            }

            for (var index = 0; index < numberOfBad; ++index)
            {
                var nextResult = new List<string>
                {
                    controllerName, route, method
                };
                IEnumerable<string> paramValues = CsprngUtils.NextRandomInt(1, numberOfParams)
                    .InvokeNTimes(badValueGenerator).FillAndShuffle(numberOfParams, goodValueGenerator);
                nextResult.AddRange(paramValues);
                nextResult.Add(100.0f.ToString(CultureInfo.InvariantCulture));
                yield return nextResult;
            }
        }

        /// <summary>
        /// Generates good training values.
        /// </summary>
        /// <returns></returns>
        public static string GoodValueGenerator() => CsprngUtils.NextAlphaString(5);

        public static string BadValueGenerator() => CsprngUtils.NextNumericString(20);

        /*
        /// <summary>
        /// Generates bad training values.
        /// </summary>
        /// <returns></returns>
        private static string BadValueGenerator() => CsprngUtils.NextRandomInt(1, 3) switch
        {
            // Strings with numbers are "abnormal"
            2 => CsprngUtils.NextNumericString(3, 10),

            // Strings with numbers and greater than length 5 are "abnormal"
            3 => CsprngUtils.NextAlphaNumericString(10, 20),

            // Empty strings are "abnormal"
            _ => string.Empty,
        };
        */
    }
}