using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ObserveAndPredict.Utils
{
    /// <summary>
    /// Static extensions for types.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global"),
     SuppressMessage("ReSharper", "UnusedType.Global")]
    public static class Extensions
    {
        /// <summary>
        /// Fills a list to the target length using the provided generator, and then shuffles the list.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="targetLength"></param>
        /// <param name="generator"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> FillAndShuffle<T>(this IEnumerable<T> enumerable, int targetLength, Func<T> generator)
        {
            List<T> generatedList = enumerable.ToList();
            while (generatedList.Count < targetLength)
            {
                generatedList.Add(generator());
            }

            return generatedList.Shuffle();
        }

        /// <summary>
        /// Given an integer and a generator method, generates values N number of times.
        /// </summary>
        /// <param name="count"></param>
        /// <param name="generator"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> InvokeNTimes<T>(this int count, Func<T> generator)
        {
            for (var index = 0; index < count; ++index)
            {
                yield return generator();
            }
        }
    }
}