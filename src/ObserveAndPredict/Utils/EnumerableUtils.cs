using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ObserveAndPredict.Utils
{
    /// <summary>
    /// Provides utilities for building and working with lists.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMember.Global"),
     SuppressMessage("ReSharper", "UnusedType.Global")]
    public static class EnumerableUtils
    {
        /// <summary>
        /// Builds a new list and fills it with N elements using the provided value generator.
        /// </summary>
        /// <param name="targetLength"></param>
        /// <param name="generator"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> NewOfGenerated<T>(int targetLength, Func<T> generator)
        {
            var result = new List<T>();
            for (var index = 0; index < targetLength; ++index)
            {
                result.Add(generator());
            }
            return result;
        }
    }
}