using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace ObserveAndPredict
{
    /// <summary>
    /// Creates a common way to describe successes and failures from actions within the
    /// server.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global"),
     SuppressMessage("ReSharper", "MemberCanBePrivate.Global"),
     SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
    public class ErrorStatus
    {
        #region Properties

        public HttpStatusCode Status { get; set; }

        public string Message { get; set; }

        #endregion Properties

        #region Constructors

        public ErrorStatus(HttpStatusCode status, string message)
        {
            Status = status;
            Message = message;
        }

        #endregion Constructors
    }
}