using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ObserveAndPredict.Models;
using ObserveAndPredict.Training;
using ObserveAndPredict.Utils;

namespace ObserveAndPredict.Controllers
{
    [ApiController]
    public class ModelController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public ModelController(ILogger<HomeController> logger) => _logger = logger;

        [HttpGet("model")]
        public IActionResult Index() => View();

        [HttpGet("model/data/generate")]
        public IActionResult GenerateTrainingData()
        {
            // TODO: This does not currently report results for "all" API methods
            var results = new List<GenerateTrainingDataViewModel>();

            foreach ((string controllerName, string route, string method, int numberOfParams,
                Func<string> goodValueGenerator, Func<string> badValueGenerator) in new[]
            {
                VulnerableController.TrainingDataBuildDefinition
            })
            {
                var individualApiMethodResults = new GenerateTrainingDataViewModel();
                results.Add(individualApiMethodResults);

                var stopwatch = Stopwatch.StartNew();
                string trainingDataPath = $"{controllerName}.{route.Replace("/", "_")}.training_data.csv";
                individualApiMethodResults.TrainingDataPath = trainingDataPath;
                individualApiMethodResults.NumberOfGoodItemsInTrainingData = 10000;
                individualApiMethodResults.NumberOfBadItemsInTrainingData = 10000;

                string headers = string.Join(",", TrainingUtils.GetTrainingHeaders(numberOfParams)) + "\n";
                string compiledContent = string.Join("\n",
                    TrainingUtils.GetTrainingValues(controllerName, route, method, numberOfParams,
                            individualApiMethodResults.NumberOfGoodItemsInTrainingData,
                            individualApiMethodResults.NumberOfBadItemsInTrainingData,
                            goodValueGenerator, badValueGenerator)
                        .Select(x => string.Join(",", x)));

                System.IO.File.WriteAllText(trainingDataPath, headers + compiledContent);
                stopwatch.Stop();
                individualApiMethodResults.MillisecondsToGenerateTrainingData = stopwatch.ElapsedMilliseconds;

                stopwatch.Reset();
                stopwatch.Start();
                string evaluationDataPath = $"{controllerName}.{route.Replace("/", "_")}.eval_data.csv";
                individualApiMethodResults.EvaluationDataPath = evaluationDataPath;
                individualApiMethodResults.NumberOfGoodItemsInEvaluationData = 1000;
                individualApiMethodResults.NumberOfBadItemsInEvaluationData = 1000;

                headers = string.Join(",", TrainingUtils.GetTrainingHeaders(numberOfParams)) + "\n";
                compiledContent = string.Join("\n",
                    TrainingUtils.GetTrainingValues(controllerName, route, method, numberOfParams,
                            individualApiMethodResults.NumberOfGoodItemsInEvaluationData,
                            individualApiMethodResults.NumberOfBadItemsInEvaluationData,
                            goodValueGenerator, badValueGenerator)
                        .Select(x => string.Join(",", x)));

                System.IO.File.WriteAllText(evaluationDataPath, headers + compiledContent);

                stopwatch.Stop();
                individualApiMethodResults.MillisecondsToGenerateEvaluationData = stopwatch.ElapsedMilliseconds;
            }

            return View(results.First());
        }

        [HttpGet("model/train")]
        public IActionResult TrainNow()
        {
            IApiModelBuilder modelBuilder =
                ApiModelBuilder<ApiModelRow, ApiModelIsSuspiciousPrediction>.Instance();

            // First, populate the training data and eval data
            ErrorStatus status = modelBuilder.LoadTrainingDataSetFromCsv(Path.Combine(Environment.CurrentDirectory,
                "VulnerableAPI.things_create.training_data.csv"));
            if (status.Status != HttpStatusCode.OK)
            {
                HttpContext.Response.StatusCode = (int) status.Status;
                return View(new TrainNowViewModel {Status = status});
            }

            status = modelBuilder.LoadEvaluationDataSetFromCsv(Path.Combine(Environment.CurrentDirectory,
                "VulnerableAPI.things_create.eval_data.csv"));
            if (status.Status != HttpStatusCode.OK)
            {
                HttpContext.Response.StatusCode = (int) status.Status;
                return View(new TrainNowViewModel {Status = status});
            }

            // Then train and evaluate the model
            status = modelBuilder.TrainModel(VulnerableController.FeatureNames, VulnerableController.LabelColumn,
                VulnerableController.HotEncodeFields);
            if (status.Status != HttpStatusCode.OK)
            {
                HttpContext.Response.StatusCode = (int) status.Status;
                return View(new TrainNowViewModel {Status = status});
            }

            HttpContext.Response.StatusCode = (int) status.Status;
            return View(new TrainNowViewModel
            {
                Status = status, ModelProperties = modelBuilder.ModelProperties,
                MillisecondsToTrainAndEvaluateModel = modelBuilder.TimeToTrainAndEvaluateModelInMs
            });
        }

        [HttpGet("model/delete")]
        public ActionResult DeleteModel()
        {
            ApiModelBuilder<ApiModelRow, ApiModelIsSuspiciousPrediction>.DeleteSingleton();
            return View(new DeleteModelViewModel
                {Status = new ErrorStatus(HttpStatusCode.OK, "If the model did exist, it sure doesn't anymore.")});
        }
    }
}