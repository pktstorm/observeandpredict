using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ObserveAndPredict.Models;
using ObserveAndPredict.Training;
using ObserveAndPredict.Utils;

namespace ObserveAndPredict.Controllers
{
    [ApiController, SuppressMessage("ReSharper", "NotAccessedField.Local")]
    public class VulnerableController : Controller
    {
        #region Fields

        private readonly ILogger<HomeController> _logger;

        #endregion Fields

        #region Properties

        public static IEnumerable<string> FeatureNames { get; } = new[]
            {"ApiName", "Route", "Method", "Param1", "Param2", "Param3", "Param4"};

        public static string LabelColumn { get; } = "IsSuspicious";

        public static IEnumerable<string> HotEncodeFields { get; } = new[]
            {"ApiName", "Route", "Method"};

        #endregion Properties

        #region Constructors

        public VulnerableController(ILogger<HomeController> logger) => _logger = logger;

        #endregion Constructors

        [HttpGet("things")]
        public IActionResult Index() => View();

        public static Tuple<string, string, string, int, Func<string>, Func<string>> TrainingDataBuildDefinition
        {
            get;
        } = Tuple.Create<string, string, string, int, Func<string>, Func<string>>("VulnerableAPI", "things/create",
            "GET", 4, () => CsprngUtils.NextRandomInt(1, 5).ToString(),
            () => CsprngUtils.NextRandomInt(6, 10000).ToString());

        [HttpGet("things/create"),
         SuppressMessage("ReSharper", "InvertIf"),
         SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        public IActionResult CreateAllTheThings(int numberOfOneFish, int numberOfTwoFish,
            int numberOfRedFish, int numberOfBlueFish)
        {
            IApiModelBuilder modelBuilder =
                ApiModelBuilder<ApiModelRow, ApiModelIsSuspiciousPrediction>.Instance();
            var thingsCreateViewModel = new ThingsCreateViewModel
            {
                IsModelAvailable = modelBuilder != null && modelBuilder.IsTrainedAndReady,
                TotalNumberOfThingCreationsRequested =
                    numberOfOneFish + numberOfTwoFish + numberOfRedFish + numberOfBlueFish
            };


            var shouldPerformTask = true;
            if (thingsCreateViewModel.IsModelAvailable)
            {
                modelBuilder.Predict(new ApiModelRow
                {
                    ApiName = "VulnerableAPI", Route = "things/create", Method = "GET",
                    Param1 = numberOfOneFish, Param2 = numberOfTwoFish, Param3 = numberOfRedFish,
                    Param4 = numberOfBlueFish
                }, out float predictionResult);
                thingsCreateViewModel.IsSuspiciousScore = predictionResult;
                thingsCreateViewModel.Threshold = 0.3f;
                if (thingsCreateViewModel.IsSuspiciousScore > thingsCreateViewModel.Threshold)
                {
                    shouldPerformTask = false;
                }
            }

            if (shouldPerformTask)
            {
                thingsCreateViewModel.CreatedThings = EnumerableUtils
                    .NewOfGenerated(numberOfOneFish + numberOfTwoFish + numberOfRedFish + numberOfBlueFish,
                        () => CsprngUtils.NextAlphaNumericString(3, 5))
                    .ToList();
                thingsCreateViewModel.WasRequestFulfilled = true;
            }

            return View(thingsCreateViewModel);
        }
    }
}