using Microsoft.ML.Data;

namespace ObserveAndPredict.Models
{
    public class TrainNowViewModel
    {
        public ErrorStatus Status { get; set; }

        public RegressionMetrics ModelProperties { get; set; }

        public long MillisecondsToTrainAndEvaluateModel { get; set; }
    }
}