namespace ObserveAndPredict.Models
{
    public class GenerateTrainingDataViewModel
    {
        public string TrainingDataPath { get; set; }

        public int NumberOfGoodItemsInTrainingData { get; set; }

        public int NumberOfBadItemsInTrainingData { get; set; }

        public long MillisecondsToGenerateTrainingData { get; set; }

        public string EvaluationDataPath { get; set; }

        public int NumberOfGoodItemsInEvaluationData { get; set; }

        public int NumberOfBadItemsInEvaluationData { get; set; }

        public long MillisecondsToGenerateEvaluationData { get; set; }
    }
}