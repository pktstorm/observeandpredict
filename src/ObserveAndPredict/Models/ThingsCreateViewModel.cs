using System.Collections.Generic;

namespace ObserveAndPredict.Models
{
    public class ThingsCreateViewModel
    {
        public int TotalNumberOfThingCreationsRequested { get; set; }
        public bool IsModelAvailable { get; set; }
        public bool WasRequestFulfilled { get; set; }
        public float IsSuspiciousScore { get; set; }
        public float Threshold { get; set; }
        public IEnumerable<string> CreatedThings { get; set; }
    }
}