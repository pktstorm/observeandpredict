# ObserveAndPredict

# Overview

The goal of ObserveAndPredict is to demonstrate the possibility of using machine-learning models to deal
with dynamically growing a rich understanding of arbitrary API interactions. Though it is not entirely generic here,
the idea is that expanding the system to arbitrarily absorb API endpoint samples would allow for the generation
of models that could be used to profile the behavior of users. That profiling could potentially indicate if users are:
* Attempting to pass bad input data to evaluate the security/stability of endpoints
* Attempting to exploit or take down services with bad input
* Interacting with calls in ways that appear to be abnormal (for example, using credentials without having requesting them)

# Composition

ObserveAndPredict is a three in one system demonstrating:
* A linux enterprise web service (ASP.NET 3.1 netcore server)
* A sensor (which collects information about API endpoint invocations)
* A security model (a machine-learning-based regression model that evaluates API endpoint invocations)

# Build and Run

## Execute

ObserveAndPredict uses a Dockerized build system to create and stage build artifacts. This is done both to avoid
requiring a consumer to have to install a number of (potentially conflicting) sdks and build tools on their
own system, as well as to maintain a consistent environment. As such, docker and docker-compse are required.

```bash
docker-compose -f docker-compose.build_and_deploy.yml up
```

Once up, point your browser at (localhost:5000)[http://localhost:5000]

# Tests

```bash
docker-compose -f docker-compose.build_and_test.yml up
```
